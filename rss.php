<?php
include "db.php";

class Rss
{
    private static $rss;

    private static function addNews($path)
    {
        self::$rss = simplexml_load_file($path);
        Db::save(self::$rss->channel->item);
    }

    private static function addAllNews($path)
    {
        self::$rss = simplexml_load_file($path);
        foreach(self::$rss->channel->item as $item)
        {
            Db::save($item);
        }
    }

    public static function add($path, $status = "update")
    {
        switch ($status) {
            case "update":
                Rss::addNews($path);
                break;
            case "all":
                Rss::addAllNews($path);
                break;
        }
    }
}