<?php
header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');

class Db
{
    private static $pdo;

    private static function connect()
    {
        $host = 'localhost';
        $db   = 'media';
        $user = 'root';
        $pass = '';
        $charset = 'utf8';
        $dsn = "sqlite:media.db";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        self::$pdo = new PDO($dsn);
    }

    public static function findAll()
    {
        self::connect();
        $sql = "SELECT * FROM feed ORDER BY `pubDate` DESC";
        $stmt = self::$pdo->query($sql);
        while ($row = $stmt->fetch())
        {
            $feed[] =
                [
                    "id" => $row['id'],
                    "hostname" => $row['hostname'],
                    "pubDate" => $row['pubDate'],
                    "link" => $row['link'],
                    "title" => $row['title'],
                    "description" => $row['description']
                ];
        }
        echo json_encode($feed);
    }

    public static function findMedia($media)
    {
        self::connect();
        $sql = "SELECT * FROM feed WHERE `hostname` = '{$media}' ORDER BY `pubDate` DESC";
        $stmt = self::$pdo->query($sql);
        while ($row = $stmt->fetch())
        {
            $feed[] =
                [
                    "id" => $row['id'],
                    "hostname" => $row['hostname'],
                    "pubDate" => $row['pubDate'],
                    "link" => $row['link'],
                    "title" => $row['title'],
                    "description" => $row['description']
                ];
        }
        echo json_encode($feed);
    }

    /*public static function findPage($page)
    {
        self::connect();
        $sql = "SELECT * FROM feed WHERE `id` = '{$page}'";
        $stmt = self::$pdo->query($sql);
        while ($row = $stmt->fetch())
        {
            $feed =
                [
                    "id" => $row['id'],
                    "hostname" => $row['hostname'],
                    "pubDate" => $row['pubDate'],
                    "link" => $row['link'],
                    "title" => $row['title'],
                    "description" => $row['description']
                ];
        }
        return json_encode($feed);
    }*/


    public static function save($item)
    {
        self::connect();
        $data = [
            'hostname' => parse_url($item->link, PHP_URL_HOST),
            'title' => $item->title,
            'link' => $item->link,
            'description' => strip_tags($item->description),
            'pubDate' => date("Y-m-d H:i:s", strtotime($item->pubDate))
        ];
        $sql = "INSERT INTO feed (hostname, title, link, description, pubDate) VALUES (:hostname, :title, :link, :description, :pubDate)";
        $stmt = self::$pdo->prepare($sql);
        $result = $stmt->execute($data);
        return $result;
    }

}
